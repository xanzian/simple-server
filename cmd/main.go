package main

import (
	"flag"
	"gitlab.com/xanzian/simple-server/internal/application"
	"log"
	"sync"
)

func main() {
	configPath := flag.String("config", "deploy/config.json", "Path to config file")
	flag.Parse()

	wg := &sync.WaitGroup{}
	app := application.NewApplication(wg)

	if err := app.Start(*configPath); err != nil {
		log.Fatalln(err)
	}

	wg.Wait()
}
