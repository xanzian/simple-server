package application

import (
	"gitlab.com/xanzian/simple-server/internal/api"
	"gitlab.com/xanzian/simple-server/internal/repository"
	"gitlab.com/xanzian/simple-server/internal/service"
	"net/http"
	"sync"

	"go.uber.org/zap"

	"gitlab.com/xanzian/simple-server/internal/config"
)

type Application struct {
	cfg     *config.Config
	logger  *zap.Logger
	router  *api.Router
	repo    *repository.FilmsRepository
	service *service.FilmService

	wgr     *sync.WaitGroup
	errChan chan error
}

func NewApplication(wg *sync.WaitGroup) *Application {
	return &Application{
		errChan: make(chan error),
		wgr:     wg,
	}
}

func (a *Application) Start(path string) (err error) {
	if err = a.initLogger(); err != nil {
		return err
	}

	if err = a.initConfig(path); err != nil {
		return err
	}

	a.repo = repository.NewFilmsRepository()
	a.service = service.NewFilmService(a.repo)

	if err = a.initRouter(); err != nil {
		return err
	}
	return nil
}

func (a *Application) initLogger() (err error) {
	if a.logger, err = zap.NewProduction(); err != nil {
		return err
	}
	return nil
}

func (a *Application) initConfig(path string) (err error) {
	if a.cfg, err = config.ParseConfig(path); err != nil {
		return err
	}
	return nil
}

func (a *Application) initRouter() (err error) {
	a.router = api.NewRouter(a.cfg, a.logger, a.service)

	if err = a.router.Start(); err != nil {
		return err
	}

	server := &http.Server{
		Addr:    ":" + a.cfg.MainPort,
		Handler: a.router.Mux,
	}

	a.wgr.Add(1)
	go func(errChan chan error) {
		defer a.wgr.Done()
		if err = server.ListenAndServe(); err != nil {
			a.logger.Error("can't start http server", zap.Error(err))
		}
	}(a.errChan) // для чего так

	a.logger.Info("Server started on port " + a.cfg.MainPort)
	return nil
}
