package service

import (
	"errors"

	"gitlab.com/xanzian/simple-server/internal/models"
)

var ErrFilmNotFound = errors.New("film not found")

type FilmRepository interface {
	Add(film models.Film)
	Get(id string) models.Film
	Delete(id string)
	GetAll() []models.Film
}

type FilmService struct {
	repo FilmRepository
}

func NewFilmService(repo FilmRepository) *FilmService {
	return &FilmService{
		repo: repo,
	}
}

func (s *FilmService) AddFilms(film models.Film) error {
	s.repo.Add(film)
	return nil
}

func (s *FilmService) GetFilmPrice(id, user string) (int, error) {
	film := s.repo.Get(id)
	if film.ID == "" {
		return 0, ErrFilmNotFound
	}

	if user == "member" {
		return film.MemberPrice, nil
	}

	return film.Price, nil
}

// тут ругалось на то что id не показан тип. Хотя в GetFilmPrice тоже не показан. Потому что в интерфейсе id, user string?
func (s *FilmService) GetFilm(id string) (models.Film, error) {
	film := s.repo.Get(id)
	if film.ID == "" {
		return film, ErrFilmNotFound
	}

	return film, nil
}

func (s *FilmService) DeleteFilm(id string) error {
	s.repo.Delete(id)

	return nil
}

func (s *FilmService) UpdateFilm(film models.Film) error {
	if filmPast := s.repo.Get(film.ID); filmPast.ID == "" {
		return ErrFilmNotFound
	}

	s.repo.Add(film)
	return nil
}

func (s *FilmService) GetAllFilms() ([]models.Film, error) {
	films := s.repo.GetAll()
	return films, nil
}
