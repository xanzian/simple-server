package repository

import (
	"gitlab.com/xanzian/simple-server/internal/models"
	"sync"
)

type FilmsRepository struct {
	Films map[string]models.Film
	mu    sync.Mutex // узнать что это race condition
}

func NewFilmsRepository() *FilmsRepository {
	return &FilmsRepository{
		Films: make(map[string]models.Film),
	}
}

// тут не нужны эрроры?
func (r *FilmsRepository) Add(film models.Film) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.Films[film.ID] = film
}

func (r *FilmsRepository) Get(id string) models.Film {
	r.mu.Lock()
	defer r.mu.Unlock()
	return r.Films[id]
}

func (r *FilmsRepository) Delete(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.Films, id)
}

func (r *FilmsRepository) GetAll() []models.Film {
	r.mu.Lock()
	defer r.mu.Unlock()

	films := make([]models.Film, len(r.Films))

	i := 0
	for _, v := range r.Films {
		films[i] = v
		i++
	}

	return films
}
