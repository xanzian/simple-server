package models

type Film struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Year        int    `json:"year"`
	Category    string `json:"category"`
	Price       int    `json:"price"`
	MemberPrice int    `json:"member_price"`
}
