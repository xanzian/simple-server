package api

import (
	"encoding/json"
	"go.uber.org/zap"
	"io"
	"net/http"
	"strconv"

	"gitlab.com/xanzian/simple-server/internal/config"
	"gitlab.com/xanzian/simple-server/internal/models"
)

// Почему если не указать в интерфейсе то не видит функцию из сервиса
// почему нет зеленой стрелки. А потом появились
// В каком порядке делать изменения - интерфейс, потом снизу вверх?
type FilmService interface {
	AddFilms(film models.Film) error
	GetFilmPrice(id, user string) (int, error)
	GetFilm(string) (models.Film, error)
	DeleteFilm(string) error
	UpdateFilm(film models.Film) error
	GetAllFilms() ([]models.Film, error)
}

type Router struct {
	Mux     *http.ServeMux
	cfg     *config.Config
	logger  *zap.Logger
	service FilmService
}

func NewRouter(cfg *config.Config, logger *zap.Logger, service FilmService) *Router {
	return &Router{
		cfg:     cfg,
		logger:  logger,
		service: service,
	}
}

func (r *Router) Start() error {
	r.Mux = http.NewServeMux()

	r.Mux.HandleFunc("/ping", r.Ping)

	// использование s при работе с одним фильмомэ
	r.Mux.HandleFunc("/films", r.AddFilms)
	r.Mux.HandleFunc("/films-price", r.GetFilmPrice)
	r.Mux.HandleFunc("/films-get", r.GetFilm)
	r.Mux.HandleFunc("/films-delete", r.DeleteFilm)
	r.Mux.HandleFunc("/films-update", r.UpdateFilm)
	r.Mux.HandleFunc("/films-get-all", r.GetAllFilms)

	// CRUD - Create, Read, Update, Delete, ReadAll
	return nil
}

func (r *Router) Ping(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("pong"))
}

func (r *Router) AddFilms(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer req.Body.Close()

	var film models.Film
	if err = json.Unmarshal(body, &film); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err = r.service.AddFilms(film); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (r *Router) GetFilmPrice(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	id := req.URL.Query().Get("id")
	user := req.URL.Query().Get("user")

	price, err := r.service.GetFilmPrice(id, user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(strconv.Itoa(price)))
}

func (r *Router) GetFilm(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	id := req.URL.Query().Get("id")

	film, err := r.service.GetFilm(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonFilm, err := json.Marshal(film)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonFilm)
}

func (r *Router) DeleteFilm(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodDelete {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	id := req.URL.Query().Get("id")

	err := r.service.DeleteFilm(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Фильм удален"))

}

func (r *Router) UpdateFilm(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPut {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer req.Body.Close()

	var film models.Film
	if err = json.Unmarshal(body, &film); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err = r.service.UpdateFilm(film); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (r *Router) GetAllFilms(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	films, err := r.service.GetAllFilms()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonFilms, err := json.Marshal(films)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonFilms)
}
