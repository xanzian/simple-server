package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	MainPort string `json:"main_port"`
}

func ParseConfig(path string) (*Config, error) {
	file, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var cfg Config
	if err = json.Unmarshal(file, &cfg); err != nil {
		return nil, err
	}
	return &cfg, nil
}
